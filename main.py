import os
from flask import Flask
from flask_cors import CORS
from gobot.v1.handlers.gobot import gobot as v1_gobot


app: Flask = Flask(__name__)
app.register_blueprint(v1_gobot, url_prefix="/v1/gobot")