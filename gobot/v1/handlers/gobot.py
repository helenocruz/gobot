from flask import Blueprint, make_response, request, make_response
from http import HTTPStatus
from werkzeug.exceptions import BadRequest
import json
from gobot.v1.services.weather import response_message


gobot = Blueprint("gobot", __name__)

@gobot.route("", methods=["POST"], endpoint="receive_message")
def receive_message():
    data = request.get_json()
    try:
        if "text" not in data:
            raise(BadRequest)

        result = response_message(data["text"])

        #print(result)

        response = make_response(result)
        response.headers["ETag"] = ""

        return response, HTTPStatus.OK

    except BadRequest as e:
        return {"message": "Problems validating request", "detailed": "The 'text' field is missing."}, HTTPStatus.BAD_REQUEST

    except Exception as e:
        return e, HTTPStatus.INTERNAL_SERVER_ERROR