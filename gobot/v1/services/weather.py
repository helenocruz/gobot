from flask import Blueprint, make_response, request, make_response
from http import HTTPStatus
from werkzeug.exceptions import BadRequest
import requests
import json


ADDRESS_API_WEATHER = 'https://weathertalk.gobots.com.br/model/parse'
ADDRESS_API_OPENWEATHER = "https://api.openweathermap.org/data/2.5/weather?units=metric&lang=pt_br&appid=809b14c511f15ce21ece2e776b56cdcf&q="
OPENWHEATHER_KEY = "809b14c511f15ce21ece2e776b56cdcf"


def response_message(message):
    data = {"text": message}
    x = requests.post(ADDRESS_API_WEATHER, json.dumps(data))
    city = _parse_response_api_gobot(x.json())
    weather = call_api_gobot(city)
    return _parse_text_return_openweather(weather)


def call_api_gobot(city):
    x = requests.get(ADDRESS_API_OPENWEATHER + city)
    return x.json()


def _parse_response_api_gobot(data):
    city = None

    entities = data.get("entities")
    for entity in entities:
        if entity['entity'] == "city":
            city = entity['value']

    return city


def _parse_text_return_openweather(data):

    description = data.get("weather")
    description = description[0].get("description")

    main = data.get("main")
    temp = main.get("feels_like")

    city = data.get("name")

    message_to_send = "O tempo na cidade " + city + ", neste momento, está " + description + " com " + str(temp) + " ºC"

    return message_to_send




   